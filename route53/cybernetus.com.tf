resource "aws_route53_zone" "cybernetus-com" {
  name = "cybernetus.com"
}

resource "aws_route53_record" "tralhas-cybernetus-com-A" {
  zone_id = aws_route53_zone.cybernetus-com.zone_id
  name    = "tralhas.cybernetus.com."
  type    = "A"
  ttl     = "1"
  records = ["69.163.220.30"]
}

resource "aws_route53_record" "protonmail-_domainkey-cybernetus-com-TXT" {
  zone_id = aws_route53_zone.cybernetus-com.zone_id
  name    = "protonmail._domainkey.cybernetus.com."
  type    = "TXT"
  ttl     = "1"
  records = ["v=DKIM1; k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDpGurqtWjR9QSsHe/ASUzd5/kGfiguPGwXNCxP4MjrOwhdlezW2lV5yGfZFWh2rEd20RAtFtqll7+JUFdDvZMPLZVQa5afM5QV8g4w1zvbyVnceFKjaSmk9fp1BS/Q0XQ8566BhILkwcB6xoK/Q9bzFyopLVjt919znrKIFBGw/wIDAQAB"]
}

resource "aws_route53_record" "my-cybernetus-com-A" {
  zone_id = aws_route53_zone.cybernetus-com.zone_id
  name    = "my.cybernetus.com."
  type    = "A"
  ttl     = "1"
  records = ["207.148.21.90"]
}

resource "aws_route53_record" "me-cybernetus-com-A" {
  zone_id = aws_route53_zone.cybernetus-com.zone_id
  name    = "me.cybernetus.com."
  type    = "A"
  ttl     = "1"
  records = ["207.148.21.90"]
}

resource "aws_route53_record" "dorimanx-cybernetus-com-A" {
  zone_id = aws_route53_zone.cybernetus-com.zone_id
  name    = "dorimanx.cybernetus.com."
  type    = "A"
  ttl     = "1"
  records = ["69.163.217.130"]
}

resource "aws_route53_record" "cybernetus-com-TXT" {
  zone_id = aws_route53_zone.cybernetus-com.zone_id
  name    = "cybernetus.com."
  type    = "TXT"
  ttl     = "1"
  records = ["brave-ledger-verification=7f7b05554a90bd6c45f189d743f72cf6fc0ed635bad24731a37d204f673d1d97", "v=spf1 include:_spf.protonmail.ch mx ~all", "protonmail-verification=a6cced9e0041451c080af01efa55ce098156115c", "keybase-site-verification=dZ9V_6KGpQosSz2pxF5FNIvJUp7e7NqnLwpW7P-502c"]
}

resource "aws_route53_record" "cybernetus-com-MX" {
  zone_id = aws_route53_zone.cybernetus-com.zone_id
  name    = "cybernetus.com."
  type    = "MX"
  ttl     = "1"
  records = ["10 mail.protonmail.ch."]
}

resource "aws_route53_record" "_dmarc-cybernetus-com-TXT" {
  zone_id = aws_route53_zone.cybernetus-com.zone_id
  name    = "_dmarc.cybernetus.com."
  type    = "TXT"
  ttl     = "1"
  records = ["v=DMARC1; p=none; rua=mailto:cybernetus@cybernetus.com"]
}

resource "aws_route53_record" "www_tralhas-cybernetus-com-A" {
  zone_id = aws_route53_zone.cybernetus-com.zone_id
  name    = "www.tralhas.cybernetus.com."
  type    = "A"
  ttl     = "1"
  records = ["69.163.220.30"]
}

