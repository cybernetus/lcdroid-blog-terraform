resource "aws_route53_zone" "loucoporandroid-com" {
  name = "loucoporandroid.com"
}

resource "aws_route53_record" "uservoice-loucoporandroid-com-A" {
  zone_id = aws_route53_zone.loucoporandroid-com.zone_id
  name    = "uservoice.loucoporandroid.com."
  type    = "A"
  ttl     = "1"
  records = ["69.163.219.3"]
}

resource "aws_route53_record" "ssh-loucoporandroid-com-A" {
  zone_id = aws_route53_zone.loucoporandroid-com.zone_id
  name    = "ssh.loucoporandroid.com."
  type    = "A"
  ttl     = "1"
  records = ["45.77.147.182"]
}

resource "aws_route53_record" "ml-_domainkey-loucoporandroid-com-TXT" {
  zone_id = aws_route53_zone.loucoporandroid-com.zone_id
  name    = "ml._domainkey.loucoporandroid.com."
  type    = "TXT"
  ttl     = "1"
  records = ["k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDKetLfIKUV0dVVTqU+HW/A+KCQwRAQGlX7lmrHW0eyiIi5LoLfNhsJOxWIID333uhuw9FMI/HYCl4hbds2Jdh5vzUhg1PIBRtJJCRTIL9ixtFQM1hLH+lx2hy5F7xdvuU5et1tFzJBkWXi585GcKGm4FCGZFpquG9ujMXCOCy5aQIDAQAB"]
}

resource "aws_route53_record" "mandrill-_domainkey-loucoporandroid-com-TXT" {
  zone_id = aws_route53_zone.loucoporandroid-com.zone_id
  name    = "mandrill._domainkey.loucoporandroid.com."
  type    = "TXT"
  ttl     = "1"
  records = ["v=DKIM1; k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCrLHiExVd55zd/IQ/J/mRwSRMAocV/hMB3jXwaHH36d9NaVynQFYV8NaWi69c1veUtRzGt7yAioXqLj7Z4TeEUoOLgrKsn8YnckGs9i3B3tVFB+Ch/4mPhXWiNfNdynHWBcPcbJ8kjEQ2U8y78dHZj1YeRXXVvWob2OaKynO8/lQIDAQAB;"]
}

resource "aws_route53_record" "mail-loucoporandroid-com-A" {
  zone_id = aws_route53_zone.loucoporandroid-com.zone_id
  name    = "mail.loucoporandroid.com."
  type    = "A"
  ttl     = "1"
  records = ["69.163.253.135"]
}

resource "aws_route53_record" "loucoporandroid-com-TXT" {
  zone_id = aws_route53_zone.loucoporandroid-com.zone_id
  name    = "loucoporandroid.com."
  type    = "TXT"
  ttl     = "1"
  records = ["keybase-site-verification=urp2DEwzphNBNtHJNNswuH9vts-L0p1h9okdxHWKlKI", "brave-ledger-verification=d03cfc25279e061c9451ca3c4f3aae10d1e8e84196b3e27aca6520b0fd445830", "google-site-verification=mQrO-uwCSjW0yu2WW_aIoIlTLD4_v7hUoTkc2XpOgI0", "v=spf1 mx a ptr include:_spf.mlsend.com include:servers.mcsv.net ~all"]
}

resource "aws_route53_record" "loucoporandroid-com-MX" {
  zone_id = aws_route53_zone.loucoporandroid-com.zone_id
  name    = "loucoporandroid.com."
  type    = "MX"
  ttl     = "1"
  records = ["10 aspmx3.googlemail.com.", "10 aspmx2.googlemail.com.", "5 alt2.aspmx.l.google.com.", "5 alt1.aspmx.l.google.com.", "1 aspmx.l.google.com."]
}

resource "aws_route53_record" "forum-loucoporandroid-com-A" {
  zone_id = aws_route53_zone.loucoporandroid-com.zone_id
  name    = "forum.loucoporandroid.com."
  type    = "A"
  ttl     = "1"
  records = ["69.163.202.249"]
}

resource "aws_route53_record" "androidapps-loucoporandroid-com-A" {
  zone_id = aws_route53_zone.loucoporandroid-com.zone_id
  name    = "androidapps.loucoporandroid.com."
  type    = "A"
  ttl     = "1"
  records = ["69.163.217.130"]
}

resource "aws_route53_record" "_c14ed52b717c94395f0d310c8086b844-www-loucoporandroid-com-CNAME" {
  zone_id = aws_route53_zone.loucoporandroid-com.zone_id
  name    = "_c14ed52b717c94395f0d310c8086b844.www.loucoporandroid.com."
  type    = "CNAME"
  ttl     = "1"
  records = ["_c2f9a16f1875053f2d1d9bb8bfcded29.nhqijqilxf.acm-validations.aws."]
}

resource "aws_route53_record" "_893021e08cab9468f4b1b2c220ffe446-loucoporandroid-com-CNAME" {
  zone_id = aws_route53_zone.loucoporandroid-com.zone_id
  name    = "_893021e08cab9468f4b1b2c220ffe446.loucoporandroid.com."
  type    = "CNAME"
  ttl     = "1"
  records = ["_67fa39819fdbbda188ac9a9402d99f7b.nhqijqilxf.acm-validations.aws."]
}
