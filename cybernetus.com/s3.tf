resource "aws_s3_bucket_public_access_block" "block-access-policy" {
  bucket = "${aws_s3_bucket.site.id}"

  block_public_acls       = true
  block_public_policy     = false
  ignore_public_acls      = false
  restrict_public_buckets = false

}

resource "aws_s3_bucket" "site" {
  bucket = "${var.domain}"
  #acl    = "public-read"
  #policy = "${data.template_file.policy.rendered}"

  website {
    index_document = "index.html"
    error_document = "404.html"
  }

}

resource aws_s3_bucket_policy "public-access" {
bucket = "${aws_s3_bucket.site.id}"

  policy = "${data.template_file.policy.rendered}"

depends_on  = [ "aws_s3_bucket_public_access_block.block-access-policy" ]

}

resource "aws_s3_bucket" "redirect" {
  bucket = "www.${var.domain}"
  acl    = "public-read"

  website {
    redirect_all_requests_to = "${var.domain}"

    # redirect_all_requests_to = "${aws_s3_bucket.site.website_endpoint}"
  }
}
