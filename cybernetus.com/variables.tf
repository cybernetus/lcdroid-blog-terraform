variable "region" {
  default = "us-east-1"
}

variable "domain" {
  default = "cybernetus.com"

}

variable "tags" {
  default     = {}
  description = "Map of the tags for all resources"
  type        = "map"
}
