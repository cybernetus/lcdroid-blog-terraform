resource "aws_s3_bucket" "log" {
  bucket = "${var.domain}-logs"
  acl    = "log-delivery-write"
}

resource "aws_s3_bucket" "site" {
  bucket = "${var.domain}"
  acl    = "public-read"
  policy = "${data.template_file.policy.rendered}"

  website {
    redirect_all_requests_to = "${var.cybernetus}"
  }

  logging {
    target_bucket = "${aws_s3_bucket.log.bucket}"
    target_prefix = "${var.domain}"
  }
}

resource "aws_s3_bucket" "redirect" {
  bucket = "www.${var.domain}"
  acl    = "public-read"

  website {
    redirect_all_requests_to = "${var.domain}"

    # redirect_all_requests_to = "${aws_s3_bucket.site.website_endpoint}"
  }
}
