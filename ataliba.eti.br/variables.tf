variable "region" {
  default = "us-east-1"
}

variable "domain" {
  default = "ataliba.eti.br"

}

variable "cybernetus" {
  default = "https://cybernetus.com"
}
