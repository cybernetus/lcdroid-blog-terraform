variable "region" {
  default = "us-east-1"
}

variable "domain" {
  default = "loucoporandroid.com"

}

variable "simple-name" {
  default = "lcdroid"
}

variable "tags" {
  default     = {}
  description = "Map of the tags for all resources"
  type        = "map"
}
